<?php

declare(strict_types=1);

namespace Thrust\Security\Contract\Exception;

class AuthorizationException extends \RuntimeException implements SecurityException
{
}