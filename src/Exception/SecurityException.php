<?php

declare(strict_types=1);

namespace Thrust\Security\Contract\Exception;

interface SecurityException
{
}