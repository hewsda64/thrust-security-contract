<?php

declare(strict_types=1);

namespace Thrust\Security\Contract\Exception;

class AuthenticationException extends \RuntimeException implements SecurityException
{
}