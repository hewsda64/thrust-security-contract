<?php

declare(strict_types=1);

namespace Thrust\Security\Contract\Value;

interface Credentials extends SecurityValue
{
    /**
     * @return mixed
     */
    public function getCredentials();
}