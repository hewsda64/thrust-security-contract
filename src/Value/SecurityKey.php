<?php

declare(strict_types=1);

namespace Thrust\Security\Contract\Value;

interface SecurityKey extends SecurityValue
{
    public function getKey(): string;
}