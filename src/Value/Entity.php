<?php

declare(strict_types=1);

namespace Thrust\Security\Contract\Value;

interface Entity
{
    public function sameIdentityAs(Entity $aEntity): bool;
}