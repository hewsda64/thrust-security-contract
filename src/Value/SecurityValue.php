<?php

declare(strict_types=1);

namespace Thrust\Security\Contract\Value;

interface SecurityValue
{
    public function sameValueAs(SecurityValue $aValue): bool;
}