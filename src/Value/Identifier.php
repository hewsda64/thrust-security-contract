<?php

declare(strict_types=1);

namespace Thrust\Security\Contract\Value;

interface Identifier extends SecurityValue
{
    /**
     * @return mixed
     */
    public function identify();
}