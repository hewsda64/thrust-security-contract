<?php

declare(strict_types=1);

namespace Thrust\Security\Contract\Token;

use Thrust\Security\Contract\Value\Credentials;
use Thrust\Security\Contract\Value\Identifier;

interface Tokenable
{
    public function roles(): array;

    public function credentials(): Credentials;

    public function setUser($user): void;

    public function user();

    public function identifier(): Identifier;

    public function isAuthenticated(): bool;

    public function setAuthenticated(bool $authenticated): void;

    public function eraseCredentials(): void;

    public function __toString(): string;
}