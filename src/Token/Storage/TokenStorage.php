<?php

declare(strict_types=1);

namespace Thrust\Security\Contract\Token\Storage;

use Thrust\Security\Contract\Token\Tokenable;

interface TokenStorage
{
    public function getToken(): ?Tokenable;

    public function setToken(Tokenable $token = null): void;
}