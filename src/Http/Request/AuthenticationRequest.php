<?php

declare(strict_types=1);

namespace Thrust\Security\Contract\Http\Request;

use Symfony\Component\HttpFoundation\RequestMatcherInterface;
use Illuminate\Http\Request as IlluminateRequest;

interface AuthenticationRequest extends RequestMatcherInterface
{
    public function extract(IlluminateRequest $request);
}