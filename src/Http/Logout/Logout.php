<?php

declare(strict_types=1);

namespace Thrust\Security\Contract\Http\Logout;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Thrust\Security\Contract\Token\Tokenable;

interface Logout
{
    public function logout(Request $request, Response $response, Tokenable $token): void;
}