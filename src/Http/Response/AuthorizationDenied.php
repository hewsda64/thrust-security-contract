<?php

declare(strict_types=1);

namespace Thrust\Security\Contract\Http\Response;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Thrust\Security\Contract\Exception\AuthorizationException;

interface AuthorizationDenied
{
    public function handle(Request $request, AuthorizationException $authorizationException) : Response;
}