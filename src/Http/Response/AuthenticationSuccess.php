<?php

declare(strict_types=1);

namespace Thrust\Security\Contract\Http\Response;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Thrust\Security\Contract\Token\Tokenable;

interface AuthenticationSuccess
{
    public function onAuthenticationSuccess(Request $request, Tokenable $token): Response;
}