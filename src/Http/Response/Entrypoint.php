<?php

declare(strict_types=1);

namespace Thrust\Security\Contract\Http\Response;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Thrust\Security\Contract\Exception\AuthenticationException;

interface Entrypoint
{
    public function start(Request $request, AuthenticationException $exception = null) : Response;
}