<?php

declare(strict_types=1);

namespace Thrust\Security\Contract\Authorization;

use Thrust\Security\Contract\Token\Tokenable;

interface Authorizable
{
    public function decide(Tokenable $token, array $attributes, $object): bool;
}