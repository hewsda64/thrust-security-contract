<?php

declare(strict_types=1);

namespace Thrust\Security\Contract\Authorization;

use Thrust\Security\Contract\Token\Tokenable;

interface Votable
{
    const ACCESS_GRANTED = 1;
    const ACCESS_ABSTAIN = 0;
    const ACCESS_DENIED = -1;

    public function vote(Tokenable $token, $subject, array $attributes): int;
}