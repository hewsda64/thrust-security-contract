<?php

declare(strict_types=1);

namespace Thrust\Security\Contract\Authentication;

use Thrust\Security\Contract\Token\Tokenable;

interface AuthenticationProvider extends Authenticatable
{
    public function supports(Tokenable $token): bool;
}