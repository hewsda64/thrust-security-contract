<?php

declare(strict_types=1);

namespace Thrust\Security\Contract\Authentication\Recaller;

use Thrust\Security\Contract\User\UserRecaller;
use Thrust\Security\Contract\Value\Identifier;
use Thrust\Security\Contract\User\User as UserContract;

interface RecallerProvider
{
    public function requireByRecallerIdentifier(Identifier $identifier, string $recallerToken): UserContract;

    public function requireByRecallerToken(string $recallerToken): UserContract;

    public function refreshRecaller(UserRecaller $user, string $newRecallerToken): UserContract;

}