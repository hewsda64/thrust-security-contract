<?php

declare(strict_types=1);

namespace Thrust\Security\Contract\Authentication;

use Thrust\Security\Contract\Token\Tokenable;

interface EnhancedTrustResolver extends TrustResolver
{
    public function isTransitional(Tokenable $token = null): bool;
}