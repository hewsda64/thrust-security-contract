<?php

declare(strict_types=1);

namespace Thrust\Security\Contract\Authentication;

use Thrust\Security\Contract\Token\Tokenable;

interface TrustResolver
{
    public function isFullFledged(Tokenable $token = null): bool;

    public function isAnonymous(Tokenable $token = null): bool;

    public function isRememberMe(Tokenable $token = null): bool;
}