<?php

declare(strict_types=1);

namespace Thrust\Security\Contract\Authentication;

use Thrust\Security\Contract\Token\Tokenable;

interface Authenticatable
{
    public function authenticate(Tokenable $token): Tokenable;
}