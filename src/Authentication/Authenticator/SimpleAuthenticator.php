<?php

declare(strict_types=1);

namespace Thrust\Security\Contract\Authentication\Authenticator;

use Thrust\Security\Contract\Token\Tokenable;
use Thrust\Security\Contract\User\UserProvider;
use Thrust\Security\Contract\Value\SecurityKey;

interface SimpleAuthenticator
{
    public function authenticateToken(Tokenable $token, UserProvider $userProvider, SecurityKey $securityKey): Tokenable;

    public function supportsToken(Tokenable $token, SecurityKey $securityKey): bool;
}