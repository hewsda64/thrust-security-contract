<?php

declare(strict_types=1);

namespace Thrust\Security\Contract\Authentication\Authenticator;

use Illuminate\Http\Request;
use Thrust\Security\Contract\Token\Tokenable;
use Thrust\Security\Contract\Value\SecurityKey;

interface SimplePreAuthenticator extends SimpleAuthenticator
{
    public function createToken(Request $request, SecurityKey $securityKey): Tokenable;
}