<?php

declare(strict_types=1);

namespace Thrust\Security\Contract\Recaller;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Thrust\Security\Contract\Token\Tokenable;

interface Recallable
{
    public function autoLogin(Request $request): ?Tokenable;

    public function loginFail(Request $request): void;

    public function loginSuccess(Request $request, Response $response, Tokenable $token): void;
}