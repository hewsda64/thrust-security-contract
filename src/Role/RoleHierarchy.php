<?php

declare(strict_types=1);

namespace Thrust\Security\Contract\Role;

interface RoleHierarchy
{
    public function getReachableRoles(array $roles): array;
}