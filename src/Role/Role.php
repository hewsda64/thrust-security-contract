<?php

declare(strict_types=1);

namespace Thrust\Security\Contract\Role;

interface Role
{
    public function getName(): string;

    public function __toString(): string;
}