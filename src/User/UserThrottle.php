<?php

declare(strict_types=1);

namespace Thrust\Security\Contract\User;

interface UserThrottle
{
    public function isEnabled(): bool;

    public function isActivated(): bool;
}