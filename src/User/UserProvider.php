<?php

declare(strict_types=1);

namespace Thrust\Security\Contract\User;

use Thrust\Security\Contract\Value\Identifier;
use Thrust\Security\Contract\User\User as SecurityUser;

interface UserProvider
{
    public function requireByIdentifier(Identifier $identifier): SecurityUser;

    public function refreshUser(SecurityUser $user): SecurityUser;

    public function supportsClass(string $class): bool;
}