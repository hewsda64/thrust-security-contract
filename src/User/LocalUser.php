<?php

declare(strict_types=1);

namespace Thrust\Security\Contract\User;

use Thrust\Security\Contract\User\Value\EncodedPassword;

interface LocalUser extends User
{
    public function getPassword(): EncodedPassword;
}