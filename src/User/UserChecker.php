<?php

declare(strict_types=1);

namespace Thrust\Security\Contract\User;
use Thrust\Security\Contract\User\User as SecurityUser;

interface UserChecker
{
    public function checkPreAuthentication(SecurityUser $user): void;

    public function checkPostAuthentication(SecurityUser $user): void;
}