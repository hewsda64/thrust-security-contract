<?php

declare(strict_types=1);

namespace Thrust\Security\Contract\User\Value;

use Thrust\Security\Contract\Value\Credentials;

interface EncodedPassword extends Credentials
{
}