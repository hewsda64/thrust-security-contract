<?php

declare(strict_types=1);

namespace Thrust\Security\Contract\User;

use Thrust\Security\Contract\User\Value\EmailAddress;
use Thrust\Security\Contract\User\Value\UserId;
use Thrust\Security\Contract\User\Value\UserName;
use Thrust\Security\Contract\Value\Identifier;

interface User
{
    public function getId(): UserId;

    public function getIdentifier(): Identifier;

    public function getUserName(): UserName;

    public function getEmail(): EmailAddress;

    public function getRoles(): array;
}