<?php

declare(strict_types=1);

namespace Thrust\Security\Contract\User;

interface UserRecaller
{
    public function getRecallerToken(): ?string;

    public function updateRecallerToken(string $token): void;
}